.. highlight:: shell

============
Installation
============


Stable release
--------------

To install Analysis Workflow, run this command in your terminal:

.. code-block:: console

    $ pip install pivana_wkfl

This is the preferred method to install Analysis Workflow, as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


From sources
------------

The sources for Analysis Workflow can be downloaded from the `Github repo`_.

You can clone the public repository:

.. code-block:: console

    $ git clone https://jaeheelee0113@bitbucket.org/bhptechsi/pivana_wkfl.git

Once you have a copy of the source, you can install it with:

.. code-block:: console

    $ python setup.py install


.. _Github repo: https://bitbucket.org/bhptechsi/pivana_wkfl/

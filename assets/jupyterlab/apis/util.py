"""
This package contains util functions.
"""
class DBUtil:
    @staticmethod
    def query_cdh(query_str):
        # @hidden_cell
        # This connection object is used to access your data and contains your credentials.
        # You might want to remove those credentials before you share your notebook.
        from project_lib import Project
        project = Project.access()
        Cloudera_Impala_JDBC_credentials = project.get_connection(name="Cloudera Impala JDBC")

        import jaydebeapi, pandas as pd
        Cloudera_Impala_JDBC_connection = jaydebeapi.connect(Cloudera_Impala_JDBC_credentials['jdbc_driver'],
                                                             Cloudera_Impala_JDBC_credentials['jdbc_url'],
                                                             [Cloudera_Impala_JDBC_credentials['username'],
                                                              Cloudera_Impala_JDBC_credentials['password']])

        result = pd.read_sql(query_str, con=Cloudera_Impala_JDBC_connection)

        Cloudera_Impala_JDBC_connection.close()

        return result
    
    def query_ora():
        print("HA")
    
    def query_mssql():
        print("HA")
    
    def query_mysql():
        print("HA")
    
    def query_tera():
        print("HA")
    
    def query_postgre():
        print("HA")
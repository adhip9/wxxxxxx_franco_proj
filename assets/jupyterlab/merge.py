'''
    Merges all datasets and hypotheses notebooks into a single master notebook.
'''
import git
import time
import os
from os import listdir
from os.path import isfile, join

if __name__ == '__main__':
    git_dir = git.Repo('.', search_parent_directories=True).working_tree_dir
    nb_dir =  git_dir + '/assets/jupyterlab/notebooks'
    doc_dir = git_dir + '/assets/jupyterlab/docs'    
    os.remove(nb_dir + '/master.ipynb') #Initially we remove the previous master notebook
    
    #Retrieve the list of datasets
    dataset_path = nb_dir + '/datasets'
    datasets = [f.replace(".ipynb","") for f in sorted(listdir(dataset_path)) if isfile(join(dataset_path, f))]
    datasets.remove('ds-template')

    #Retrieve the list of hypothesis
    hypotheses_path = nb_dir + '/hypotheses'
    hypotheses = [f.replace(".ipynb","") for f in sorted(listdir(hypotheses_path)) if isfile(join(hypotheses_path, f))]
    hypotheses.remove('hs-template')
    
    
    #For each dataset, grab the list of related hypotheses and merge them.
    if not len(datasets) == 0:
        os.remove(nb_dir + '/merged/dataset-merged.ipynb') #We first remove dataset-merged.
        command_string = 'nbmerge '
        for dataset in datasets:
            command_string = command_string + dataset_path + '/' + dataset + '.ipynb '
        command_string = command_string + ' > ' + nb_dir + '/merged/dataset-merged.ipynb'
        print("Executing " + command_string)
        os.system(command_string)
        print("DONE")
    
    if not len(hypotheses) == 0:
        os.remove(nb_dir + '/merged/hypothesis-merged.ipynb') #We first remove hypothesis-merged.
        command_string = 'nbmerge '
        for hypothesis in hypotheses:
            command_string = command_string + hypotheses_path + '/' + hypothesis + '.ipynb '
        command_string = command_string + ' > ' + nb_dir + '/merged/hypothesis-merged.ipynb'
        print("Executing " + command_string)
        os.system(command_string)
        print("DONE")
    
    #now merge all notebooks inside merged file into master.ipynb
    merged_path = nb_dir + '/merged'
    merged = []
    for file in sorted(os.listdir(merged_path)):
        if not file == '.ipynb_checkpoints':
            merged.append(merged_path + '/' + file)
    merged_string = " ".join(merged)
    command_string = 'nbmerge ' + nb_dir + '/master-init.ipynb ' + merged_string + ' > ' + nb_dir + '/master.ipynb'
    print("Executing " + command_string)
    os.system(command_string)
    print("DONE")
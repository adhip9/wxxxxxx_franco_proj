======================
Analysis Workflow
======================

The project used for the bottleneck analysis. Users can play with this repository as this is not a production repository.

Commands used for the workflow
------------------------------

**Assumption**: you are in a git repository. Try ‘git status’ to see whether you are in a git repository. If not, go to the directory.

**Installing dependencies**

* pip install -r requirements-dev.txt
* pre-commit install

**Git commands**

* **git clone <repository>**: clones (makes a local copy of) the repository 
* **git status**: displays the state of the working directory and the staging area. It lets you see which changes have been staged (for commit), which haven’t and which are not being tracked by Git. It also shows the current branch you are on.
* **git checkout <branch>**: switches the current branch to the branch you specify.
* **git add <files>**: add files to the staging area for commit
* **git commit -m "message"**: captures a snapshot of the project's currently staged changes. This will not affect the repository unless you push the commit.
* **git push**: uploads the local repository content (a set of commit(s)) to the remote repository (server).
* **git pull**: downloads the remote repository content to the local repository. You do this to download contents that some other users have worked on.

For more information, type 'git help.' Also, read: `Feature Branch Workflow by Atlassian <https://www.atlassian.com/git/tutorials/comparing-workflows/feature-branch-workflow>`_



Features
--------

* Master notebook generation / update
* API Documentation generation / update

Credits
-------

* The core developer is Jae Lee (jaehee.lee@bhp.com)
* This package was created with Cookiecutter_ and the `audreyr/cookiecutter-pypackage`_ project template.

.. _Cookiecutter: https://github.com/audreyr/cookiecutter
.. _`audreyr/cookiecutter-pypackage`: https://github.com/audreyr/cookiecutter-pypackage
